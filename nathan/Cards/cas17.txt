Title
Scot-Free


Class
Cassius


Level
2


Type
Power


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-7z8ZG52/0/f98d7ee5/O/download%20%2812%29.png


Quantity
1


Spark
---


Description



Keep
FALSE


Trigger
Reaction


TriggerAction
If an **Enemy** dies during any Enemy's turn within ^Range^2, gain ^Stealth^


Print
NO


Public
YES


id
cas17


