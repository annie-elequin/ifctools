Title
Vantage Point


Class
Dreya


Level
1


Type
Card


Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/dreya_vantagepoint/quality=100


Quantity
1


Spark
1


Description
Scatter 2 with^Jump^Jump. <br/>If your ^Next^Next card deals Damage, gain ^Crit^Crit for all damage


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
dre26


