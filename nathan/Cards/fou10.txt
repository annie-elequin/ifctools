Title
Sharply Dressed


Class
Foulborn


Level
0


Type
Card


Scale
20


Image
https://www.pngrepo.com/png/321435/180/spiked-armor.png


Quantity
1


Spark
1


Description
Gain (2+^spX^) ^Backlash^**Backlash** <br/>until the end of the Round<br/><br/>(^Range^2): ^Taunt^Taunt 1


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
fou10


