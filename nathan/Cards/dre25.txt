Title
^Arrow^Cruel Arrow


Class
Dreya


Level
1


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-KWFpTb5/0/9f95e2ba/O/dreya_cruelarrow.png


Quantity
1


Spark
1


Description
(^Range^2/4): ^d5^ <br/> When played, this abilitiy must Target the Enemy in Range with the least HP


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
dre25


