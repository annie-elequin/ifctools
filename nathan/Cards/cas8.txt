Title
Nothing to See


Class
Cassius


Level
3


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-hNHHQ4M/0/aef7f79c/O/download%20%2818%29.png


Quantity
1


Spark
1


Description
^Discard^Discard 0-4 from your hand <br/> ^b3^ for each Card Discarded<br/><br/>^Draw^Draw until you Gain at least <br/>(1+^spX^) ^Spark^Spark


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
cas8


