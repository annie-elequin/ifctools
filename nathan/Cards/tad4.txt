Title
(Bot) Mechasuit


Class
Tad


Level
1


Type
Card


Scale
0


Image
https://cdn2.iconfinder.com/data/icons/mech-mecha-robot/152/mech-robot-5-512.png


Quantity
1


Spark
0


Description
^KeepTap^**Keep** = ^Tuck^**Tuck 1**


Keep
TRUE


Trigger
Tuck


TriggerAction
Gain ^bX^ equal to half the^Spark^in this Stack. If the Block gained is **6** or more, immediately ^Cast^ this **Stack**into your **Discard** pile.


Print
NO


Public
NO


id
tad4


