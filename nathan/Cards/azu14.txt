Title
Marksman's Slug


Class
Azura


Level
2


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Azura/i-wK6dt6f/0/6b6aa901/O/icon_4017847_edited.png


Quantity
1


Spark
1


Description
^KeepTap^**Keep** = (^Line^2/3): ^d2^


Keep
TRUE


Trigger
Cast


TriggerAction
Gain 1 ^Flex^**Flex** and return this to your hand


Print
NO


Public
NO


id
azu14


