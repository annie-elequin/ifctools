Title
Crusher


Class
Foulborn


Level
2


Type
Card


Scale
20


Image
https://www.pngrepo.com/png/321885/180/boulder-dash.png


Quantity
1


Spark
2


Description
(^Range^2): ^d3^ <br/>On ^Hit^Hit, ^b2^ <br/><br/>If the Target loses 0 HP, you may <br/>Lose 3 ^b^ to ^Loop^Loop this card


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
fou15


