Title
Sabotage


Class
Cassius


Level
1


Type
Card


Scale
30


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-WjDgcPf/0/938e8cec/O/download%20%2829%29.png


Quantity
1


Spark
1


Description
(^Range^2): Steal ^b3^<br/><br/>OR<br/><br/>^Disarm^Disarm (1+^spOne^)


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
cas13


