Title
Power Stance


Class
Foulborn


Level
0


Type
Card


Scale
20


Image
https://www.pngrepo.com/png/158551/180/hands-on-hips-silhouette.png


Quantity
1


Spark
1


Description
^Pull^Pull 1-2 to <br/>any Unit within (^Range^3)


Keep
FALSE


Trigger
Reaction


TriggerAction
^b4^<br/>(^Range^2):^Taunt^Taunt 1


Print
NO


Public
YES


id
fou5


