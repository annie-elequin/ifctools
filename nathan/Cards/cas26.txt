Title
Sneak Attack


Class
Cassius


Level
2


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-HBqPZrB/0/f7f3bf14/O/download%20%2828%29.png


Quantity
1


Spark
2


Description
^KeepTap^**Keep** = ^Tuck^Tuck 1 and Lose HP equal to its ^Spark^Spark


Keep
TRUE


Trigger
Reaction


TriggerAction
If an Enemy willingly Moves into an adjacent space, you may Discard this stack and<br/> ^dX^ equal to the <br/>total Spark in this Stack


Print
NO


Public
YES


id
cas26


