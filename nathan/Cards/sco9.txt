Title
Predator


Class
Scourge


Level
1


Type
Card


Scale
0


Image
https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRPDZDCZSMhnkr4kKtPzhH8uT2MR4fWAttTvqJwTANWeeeapMuRdMwJ93s5beRlRKiEEOc&usqp=CAU


Quantity
1


Spark
1


Description
Find the Enemy with the lowest HP. ^Move^Move 2 along the shortest path towards them


Keep
FALSE


Trigger
Reaction


TriggerAction
If an Enemy within (^Range^4) has just lost **HP**,^Jump^Jump to the closest unoccupied space that is adjacent to them


Print
NO


Public
YES


id
sco9


