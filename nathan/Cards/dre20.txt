Title
^Arrow^Volley Arrow


Class
Dreya


Level
1


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-xXWZhpb/0/78a21b0e/O/Scattering%20Arrow.png


Quantity
1


Spark
1


Description
<img src="https://i.ibb.co/pRJJhK0/Dreya-Scattering-Arrow.png" alt="Dreya-Scattering-Arrow" border="0" width="450" height="400"> <br/>


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
dre20


