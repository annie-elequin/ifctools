Title
Nightcrawler


Class
Foulborn


Level
2


Type
Card


Scale
40


Image
https://www.pngrepo.com/png/114947/180/halloween-bleeding-moon.png


Quantity
1


Spark
1


Description
^Move^Move X equal to half of your current Initiative (rounded down),<br/> up to 4


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
fou16


