Title
Secret Compartment


Class
Cassius


Level
2


Type
Card


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-7pPCXRc/0/0d9fa7fd/O/download%20%2826%29.png


Quantity
1


Spark
2


Description
^KeepTap^**Keep** = ^Tuck^ **Tuck 1**


Keep
TRUE


Trigger
Reaction


TriggerAction
Return this **Stack** to your hand


Print
NO


Public
YES


id
cas24


