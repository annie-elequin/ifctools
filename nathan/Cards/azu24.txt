Title
Horizon Rift


Class
Azura


Level
4


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Azura/i-2Ft5dGm/0/58dab3e8/O/Horizon%20Rift.png


Quantity
1


Spark
2


Description
^Lob^ **3** Place a Special Token on an unoccupied space and remove it at the start of your next turn. Until the end of your turn you may start all ^Line^ attacks from this token (including the space itself)


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
NO


id
azu24


