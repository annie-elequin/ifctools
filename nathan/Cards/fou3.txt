Title
Double Flail


Class
Foulborn


Level
0


Type
Card


Scale
40


Image
https://static.thenounproject.com/png/495569-200.png


Quantity
1


Spark
2


Description
(^Range^ 2/2 ^Target^**Target 2**): ^d4^<br/><br/>(Range 2/2: Targets must be exactly 2 spaces away)


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
fou3


