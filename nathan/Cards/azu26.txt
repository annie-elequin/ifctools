Title
High-Caliber Mark


Class
Azura


Level
1


Type
Power


Scale
10


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Azura/i-kw7P7hG/0/52ef9911/O/icon_366050_edited.png


Quantity
1


Spark
---


Description



Keep
FALSE


Trigger
Reaction


TriggerAction
Lose 1 to all **Range Values** (min. 1) this Round and <br/>Gain (1+^spX^**Flex**)


Print
NO


Public
NO


id
azu26


