import React, { useContext } from 'react'
import CardImage from '../Print/CardImage'
import Card from '../../components/HtmlCard';
import { AppContext } from '../../context/AppContext';
import Header from '../../Header';

export default function Test() {
  const { state: { publicAbilities: abilities, regex }} = useContext(AppContext);

  if (abilities.length === 0 || !regex) return null;

  console.log({abilities})

  const renderCardPages = () => {
    return (
      <CardImage
        data={[abilities[20]]}
        regex={regex}
        curPage={1}
        clearElements={clearElements}
      />
    );
  };

  const clearElements = () => {
    abilities.forEach((card, i) => {
      const el = document.getElementById((card as any).id);
      if (el) {
        el.remove();
      }
    });
  };


  return (
    <div>
      <Header /> 
      <Card card={abilities[20]} regex={regex} />
      {renderCardPages()}
    </div>
  )
}