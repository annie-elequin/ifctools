import { useContext, useState } from "react";
import Header from "../../Header";
import { AppContext } from "../../context/AppContext";
import SelectSearch from "react-select-search";
import CardImage from "../Print2/CardPages";

export default function Canvas() {
  const {
    state: { publicAbilities: rawAbilities, regex },
  } = useContext(AppContext);
  const [currentClass, setCurrentClass] = useState();
  const [hand, setHand] = useState([]);

  let abilities = rawAbilities;

  const getOptions = () => {
    const options: any[] = [];
    abilities.forEach((card) => {
      const existing = options.find((o) => o.value === card.Class);
      if (!existing) {
        options.push({
          name: card.Class,
          value: card.Class,
        });
      }
    });

    return options;
  };

  const onChangeCard = (classValue) => {
    setCurrentClass(classValue);
    const newHand: any = abilities
      .filter((a) => a.Class === classValue)
      .filter((a) => a.Print === "YES");
    setHand(newHand);
  };

  const renderDeck = () => {
    if (abilities.length === 0 || !regex || hand?.length === 0) return null;

    console.log("Rendering abilities...", { hand });

    return (
      <CardImage
        data={hand}
        regex={regex}
        curPage={0}
        clearElements={clearElements}
        column={`${8}`}
        row={`${Math.ceil(hand.length / 8)}`}
      />
    );
  };

  const clearElements = () => {
    abilities.forEach((card, i) => {
      const title = document.getElementById(`title-${(card as any).id}`);
      const el = document.getElementById((card as any).id);
      if (el) {
        el.remove();
      }
      if (title) {
        title.remove();
      }
    });
  };

  return (
    <div>
      <Header />
      <SelectSearch
        options={getOptions()}
        search={true}
        value={currentClass}
        placeholder="Choose or search for a Mercenary"
        onChange={onChangeCard}
      />
      <div
        style={{
          flexDirection: "column",
          padding: 24,
        }}
      >
        {renderDeck()}
      </div>
    </div>
  );
}
