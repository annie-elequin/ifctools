import React, { useContext, useState } from "react";
import { AppContext } from "../../context/AppContext";
import Header from "../../Header";
import SelectSearch from "react-select-search";
import "react-select-search/style.css";
import { Button, FormInput } from "shards-react";
import { shuffleArray } from "../../utility";
import Card from "../../components/HtmlCard";

export default function HandSimulator() {
  const {
    state: { publicAbilities: rawAbilities, regex },
  } = useContext(AppContext);
  const [currentClass, setCurrentClass] = useState();
  const [hand, setHand] = useState([]);
  const [visible, setVisible] = useState([]);
  const [discard, setDiscard] = useState([]);
  const [spark, setSpark] = useState('5');
  const [level, setLevel] = useState('')
  const [isDiscardOpen, setIsDiscardOpen] = useState(false);

  let abilities = rawAbilities.filter(c => c.Type === 'Card')
  if (level?.length > 0) {
      abilities = abilities.filter(c => c.Level === level);
  }

  const toggleDiscard = () => {
    setIsDiscardOpen(!isDiscardOpen);
  };

  const getOptions = () => {
    const options: any[] = [];
    abilities.forEach((card) => {
      const existing = options.find((o) => o.value === card.Class);
      if (!existing) {
        options.push({
          name: card.Class,
          value: card.Class,
        });
      }
    });

    return options;
  };

  const onChangeCard = (classValue) => {
    console.log(classValue);
    setCurrentClass(classValue);
    const newHand: any = abilities.filter((a) => a.Class === classValue);
    shuffleArray(newHand);
    setHand(newHand);
  };

  const reshuffle = () => {
    const newHand = [...hand, ...visible, ...discard];
    shuffleArray(newHand);
    setHand(newHand);
    setVisible([]);
    setDiscard([]);
  };

  const promptReset = () => {
    const isSure = window.confirm("Are you sure you want to reset the deck?");
    if (isSure) {
      reshuffle();
      onChangeCard(currentClass);
    }
  };

  const draw = () => {
    let newDiscard = [...discard, ...visible];
    let sparkValue: any = 0;
    let handCopy = [...hand];
    const newVisible: any = [];
    while (sparkValue < parseInt(spark)) {
      const item: any = handCopy.pop();
      if (!item) {
        handCopy = [...newDiscard];
        shuffleArray(handCopy);
        const thing: any = handCopy.pop();
        sparkValue += parseInt(thing.Spark);
        newVisible.push(thing);
        newDiscard = [];
        continue;
      }
      sparkValue += parseInt(item.Spark) || 0;
      newVisible.push(item);
    }
    setDiscard(newDiscard);
    setHand(handCopy);
    setVisible(newVisible);
  };

  return (
    <div>
      <Header />
      <div
        style={{
          width: "100%",
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
          marginTop: 48,
        }}
      >
        <FormInput placeholder="Spark" value={spark} onChange={ev => setSpark(ev.target.value)} style={{ width: 100 }} />
        <FormInput placeholder="Level" value={level} onChange={ev => setLevel(ev.target.value)} style={{ width: 100 }} />
        <SelectSearch
          options={getOptions()}
          search={true}
          value={currentClass}
          placeholder="Choose or search for a card"
          onChange={onChangeCard}
        />
        <div style={{ width: 50 }} />
        <div style={{ width: 50 }} />
        <Button theme="danger" onClick={promptReset}>
          Reset
        </Button>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          marginTop: 48,
        }}
      >
        <CardPile label={`${hand.length} Cards`} onClick={draw} />
        <div style={{ display: "flex", flexDirection: "row", flexWrap: 'wrap'  }}>
          {visible.map((c) => (
            <Card card={c} regex={regex} />
          ))}
        </div>
        {discard.length > 0 && (
          <CardPile
            label={`${discard.length} Discard`}
            onClick={toggleDiscard}
          />
        )}
        <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}>
        { isDiscardOpen && (
        discard.map((c) => (
          <Card card={c} regex={regex} />
        ))
      )}
        </div>
      </div>
    </div>
  );
}

function CardPile({ label, onClick }) {
  return (
    <div
      style={{
        width: 250,
        height: 350,
        backgroundColor: "teal",
        borderRadius: 15,
        cursor: "pointer",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        color: "white",
      }}
      onClick={onClick}
    >
      {label}
    </div>
  );
}
