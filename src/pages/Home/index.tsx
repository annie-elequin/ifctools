import React from "react";
import { Link } from "wouter";
import Header from "../../Header";
import { Button } from "shards-react";

export default function Home() {
  const pages = [
    {
      link: "/print",
      label: "Print Cards",
      description:
        "Generates A4 pages of 8 cards each suitable for printing prototype cards.",
    },
    { link: "/cards", label: "View Cards", description: "Just want to see aaaaalllll the cards? Go here!" },
    { link: "/compare", label: "Compare Cards", description: "If Nathan has set it up, he can compare cards between different versions he has created." },
    { link: "/history", label: "View Card History", description: "If available, you can view the history of changes for a given card." },
    { link: "/tts", label: "Tabletop Simulator Print", description: "Generates an entire deck for a character on one page, suitable for TTS import." },
  ];

  return (
    <div>
      <Header />
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          flexWrap: "wrap",
          justifyContent: "center",
          textAlign: "center",
          color: "#fff",
          marginTop: 60,
          fontSize: 30,
        }}
      >
        Welcome to IFC Tools!
        <br />
        Here you can browse a variety of tools Nathan uses to develop IFC.
        <div style={{ fontSize: '.6em', marginTop: 30 }}>
          {pages.map((p) => (
            <ButtonGroup key={p.label} {...p} />
          ))}
        </div>
      </div>
    </div>
  );
}

function ButtonGroup({ link, label, description }) {
  return (
    <div
      style={{ display: "flex", flexDirection: "row", alignItems: "center", marginBottom: 12 }}
    >
      <Link href={link}>
        <Button style={{ width: 250 }}>{label}</Button>
      </Link>
      <div style={{ marginLeft: 20, color: "white" }}>{description}</div>
    </div>
  );
}
