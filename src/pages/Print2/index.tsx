import React, { useContext, useEffect, useState } from "react";
import { Button } from "shards-react";
import Header from "../../Header";
import CardImage from "./CardPages";
import { AppContext } from "../../context/AppContext";

export default function Canvas() {
  const [pages, setPages] = useState(0);
  const [curPage, setCurPage] = useState(0);
  const {
    state: { publicAbilities: unfilteredAbilities, regex },
  } = useContext(AppContext);
  const [abilities, setAbilities] = useState([]);

  const renderCardPages = () => {
    if (abilities.length === 0 || !regex) return null;
    const maxIndex =
      curPage * 8 + 8 > abilities.length ? abilities.length : curPage * 8 + 8;
    const cardsForPage = abilities.slice(curPage * 8, maxIndex);
    console.log({ cardsForPage });
    return (
      <CardImage
        data={cardsForPage}
        regex={regex}
        curPage={curPage}
        clearElements={clearElements}
      />
    );
  };

  const clearElements = () => {
    abilities.forEach((card, i) => {
      const title = document.getElementById(`title-${(card as any).id}`);
      const el = document.getElementById((card as any).id);
      if (el) {
        el.remove();
      }
      if (title) {
        title.remove();
      }
    });
  };

  useEffect(() => {
    const filtered = unfilteredAbilities.filter(
      (card) => card["Print"] === "YES"
    );
    setAbilities(filtered as any);
  }, [unfilteredAbilities]);

  useEffect(() => {
    const p = Math.ceil(((abilities as any) || [])?.length / 8);
    setPages(p);
  }, [abilities]);

  return (
    <div style={{ color: "white" }}>
      <Header />
      <div
        style={{
          flexDirection: "column",
          padding: 24,
        }}
      >
        <div style={{ marginBottom: 24 }}>
          <a
            style={{ width: 150, textAlign: "center", alignSelf: "center" }}
            href="https://aelequin.notion.site/Why-aren-t-my-images-showing-up-in-the-Render-6a4082fc892f406b824ddb9dff7df0fb"
            // target="_blank"
            rel="noopener noreferrer"
          >
            Why aren't my images showing up?
          </a>
        </div>
        <h4 style={{ color: "white" }}>
          Page {curPage + 1} of {pages}
        </h4>
        <div style={{ flexDirection: "row", alignItems: "center" }}>
          <Button
            style={{ marginTop: 12, marginBottom: 24, marginRight: 12 }}
            onClick={() => setCurPage(curPage - 1)}
            disabled={curPage === 0}
          >
            Prev
          </Button>
          <Button
            style={{ marginTop: 12, marginBottom: 24 }}
            onClick={() => setCurPage(curPage + 1)}
            disabled={curPage === pages}
          >
            Next
          </Button>
        </div>
        {renderCardPages()}
      </div>
    </div>
  );
}
