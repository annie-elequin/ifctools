import React, { useContext, useState } from "react";
import { useLocation, useRoute } from "wouter";
import { AppContext } from "../../context/AppContext";
import Header from "../../Header";
import SelectSearch from "react-select-search";
import "react-select-search/style.css";
import CardHistory from "./CardHistory";

export default function History() {
  const {
    state: { nonDuplicatedAbilities: abilities },
  } = useContext(AppContext);
  const [, params] = useRoute("/history/:cardId");
  const [, setLocation] = useLocation();
  const [cardValue, setCardValue] = useState(params?.cardId);

  console.log({abilities})

  const getOptions = () => {
    const options: any[] = [];
    const currentGroup: { type: string; items?: any; name?: string } = {
      type: "group",
    };
    abilities.forEach((card) => {
      if (card.Class === currentGroup.name) {
        // add card to group
        currentGroup.items.push({
          name: card.Title,
          value: card.id,
        });
      } else {
        if (currentGroup.name) {
          // add to options
          const newGroup = { ...currentGroup };
          options.push(newGroup);
        }
        currentGroup.name = card.Class;
        currentGroup.items = [
          {
            name: card.Title,
            value: card.id,
          },
        ];
      }
    });
    if (abilities?.length > 0) {
      options.push(currentGroup)
    }
    return options;
  };

  const onChangeCard = (cardId) => {
    setLocation(`/history/${cardId}`)
    setCardValue(cardId);
  }

  return (
    <div>
      <Header />
      <div
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "center",
          marginTop: 48,
        }}
      >
        <SelectSearch
          options={getOptions()}
          search={true}
          value={cardValue}
          placeholder="Choose or search for a card"
          onChange={onChangeCard}
        />
      </div>
      {params?.cardId && (
        <CardHistory />
      )}
    </div>
  );
}
