import React, { useContext, useEffect, useState } from "react";
import { useLocation, useRoute } from "wouter";
import { AppContext } from "../../context/AppContext";
import Header from "../../Header";
import SelectSearch from "react-select-search";
import "react-select-search/style.css";
import { fetchIfcCompareAbilities } from "../../api";
import Card from "../../components/HtmlCard";
import { transformCardStyle } from "../../utility/styles";

export default function Compare() {
  const {
    state: { abilities, regex },
  } = useContext(AppContext);
  const [, params] = useRoute("/compare/:cardId");
  const [, setLocation] = useLocation();
  const [cardValue, setCardValue] = useState(params?.cardId);
  const [compareCards, setCompareCards] = useState();

  const getOptions = () => {
    const options: any[] = [];
    const currentGroup: { type: string; items?: any; name?: string } = {
      type: "group",
    };
    const addedAbilities = {};
    abilities.forEach((card) => {
      if (addedAbilities[card.id]) return;
      if (card.Class === currentGroup.name) {
        // add card to group
        addedAbilities[card.id] = true;
        currentGroup.items.push({
          name: card.Title,
          value: card.id,
        });
      } else {
        if (currentGroup.name) {
          // add to options
          const newGroup = { ...currentGroup };
          options.push(newGroup);
        }
        currentGroup.name = card.Class;
        currentGroup.items = [
          {
            name: card.Title,
            value: card.id,
          },
        ];
        addedAbilities[card.id] = true;
      }
    });
    if (abilities?.length > 0) {
      options.push(currentGroup);
    }
    return options;
  };

  const onChangeCard = (cardId) => {
    setLocation(`/compare/${cardId}`);
    setCardValue(cardId);
  };

  const getComparedCards = async () => {
    const cards = await fetchIfcCompareAbilities();
    setCompareCards(cards as any);
  };

  useEffect(() => {
    getComparedCards();
  }, []);

  return (
    <div>
      <Header />
      <div
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "center",
          marginTop: 48,
        }}
      >
        <SelectSearch
          options={getOptions()}
          search={true}
          value={cardValue}
          placeholder="Choose or search for a card"
          onChange={onChangeCard}
        />
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          flexWrap: "wrap",
          margin: 24,
          ...transformCardStyle
        }}
      >
        {params?.cardId && abilities?.length > 0 && (
          <Card card={abilities?.filter(a => a.id === params.cardId)?.[0]} regex={regex} />
        )}
        {params?.cardId && abilities?.length > 0 &&
          (compareCards || ([] as any))
            ?.filter((c) => c.id.includes(`${cardValue}_`))
            ?.map((c) => <Card card={c} regex={regex} />)}
      </div>
    </div>
  );
}
