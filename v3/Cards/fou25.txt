Title
Neurotoxin


Class
Foulborn


Order
X


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-3hKMQtz/0/21d58cff/O/download%20%288%29.png


Quantity
1


Spark
1


Cost
T


Power
1


OnSlot



OnUse
^Taunt^Taunt up to (1+^SparkX^) <br/>Enemies with any ^p^Poison within (Range^Range^4)<br/><br/>After they act, ^Blind^Blind them


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
NO


Public
NO


id
fou25


Speed
0


Tactics
0


