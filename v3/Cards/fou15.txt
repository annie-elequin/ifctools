Title
Clobber


Class
Foulborn


Order
15


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-kQNr5cF/0/a787f7b1/X2/3034092-X2.png


Quantity
1


Spark
2


Cost
PP


Power
1


OnSlot



OnUse
<img src="https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/FoulbornAOEClobber/quality" width="500" height="500">
<br/>^Disarm^Disarm any Target that lost 0 HP


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
fou15


Speed
0


Tactics
0


