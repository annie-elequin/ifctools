Title
Fusion Reactor


Class
Scourge


Level
2


Type
Card


Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/scourge_fusionreactor/public


Quantity
1


Spark
1


Description
Set your HP to equal your Missing HP. Depending on the result:<br/><br/>^Range^2 ^dX^ <br/>equal to the HP you lost<br/>OR<br/>^Move^Move X<br/>up to the HP you gained


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
sco34


