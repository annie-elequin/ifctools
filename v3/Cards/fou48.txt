Title
Stone Skin


Class
Foulborn


Order
X


Level



Scale
0


Image



Quantity
1


Spark
1


Cost



Power
2


OnSlot
You must spend ^b3^ to Slot this
<br/>
<br/>^Draw^ Draw 1


OnUse
^hX^ Heal equal to half your ^b^Block (rounded down)


Reaction



ReactionAction



Special
While Slotted


SpecialAction
Every time you receive ^d^Damage, you may choose to reduce it by 1


Description



Trigger



TriggerAction



Print
NO


Public
NO


id
fou48


Speed
0


Tactics
0


