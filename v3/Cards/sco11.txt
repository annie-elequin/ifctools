Title
Simmer


Class
Scourge


Order
2


Level
S


Scale
0


Image
https://cdn-icons-png.flaticon.com/512/1689/1689090.png


Quantity
1


Spark
0


Cost



Power
1


OnSlot



OnUse
Lose 0-3 ^Rage^Rage Tokens and give them to any Players as ^Spark^Spark
<br/>
<br/>For each Token you gave to another Player, ^h2^ Heal 2


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
sco11


Speed
0


Tactics
0






