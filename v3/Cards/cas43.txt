Title
Tactical Stab


Class
Cassius


Order
X


Level



Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-NXNSst9/0/48b680d5/O/Tactical%20Stab.png


Quantity
1


Spark
3


Cost
2


Power
2


OnSlot



OnUse
(Range^Range^1): Deal ^dX^ Damage, equal to the amount of ^Spark^Spark currently in your supply
<br/>
<br/>On ^Hit^Hit, Gain ^Spark^Spark


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
NO


Public
NO


id
cas43


Speed
0


Tactics
0


