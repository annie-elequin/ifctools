Title
Cruel Arrow


Class
Dreya


Order
19


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-KWFpTb5/0/9f95e2ba/O/dreya_cruelarrow.png


Quantity
1


Spark
1


Cost
1


Power
1


OnSlot



OnUse
(Range ▲): Deal ^d3^ Damage
<br/>If Target has half or less HP, deal this damage Twice
<br/>
<br/>^SparkOne^= ^Topdeck^Topdeck this card


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
dre26


Speed
0


Tactics
0






d
d


https://i0.wp.com/www.atlaslisboa.com/wp-content/uploads/2017/05/wolf-trap-icon-10.png
https://i0.wp.com/www.atlaslisboa.com/wp-content/uploads/2017/05/wolf-trap-icon-10.png


S
S


