Title
Antidote


Class
Cassius


Order
X


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-8kkZf8s/0/725c18fe/O/Antidote.png


Quantity
1


Spark
1


Cost
1


Power
0


OnSlot



OnUse
Remove up to ^p6^Poison from any Unit within (^Range^2)<br/><br/>For each ^p2^ Poison removed, you may:^h2^Heal or ^Draw^Draw 1


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
NO


Public
NO


id
cas49


Speed
0


Tactics
0


