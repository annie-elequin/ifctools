Title
Bag of Blades


Class
Cassius


Order
X


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-7JxNXjJ/0/fc5eac3e/O/Bag%20of%20Blades.png


Quantity
1


Spark
1


Cost
1


Power
0


OnSlot



OnUse



Reaction
End-Round


ReactionAction
Clear this card if it has
<br/>5 or more ^Spark^ on it


Special
Always


SpecialAction
At any time you may place ^SparkOne^ on this card to perform the following:<br/>(^Range^1):^d1^ 
<br/>If this attack kills, 
<br/>Refund the ^Spark^ to your supply


Description



Trigger



TriggerAction



Print
NO


Public
NO


id
cas50


Speed
0


Tactics
0


