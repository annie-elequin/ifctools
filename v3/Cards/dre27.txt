Title
Soaring Arrow


Class
Dreya


Order
20


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-sc25vHj/0/ff57d565/O/arrow-hunting-weapon-icon.png


Quantity
1


Spark
1


Cost
1


Power
1


OnSlot



OnUse
(Range ▲): Deal ^dX^ Damage equal to your Distance from the Target 
<br/>
<br/>^SparkOne^= Add 1-2 to this Range
<br/>^SparkTwo^= Add 3-4 to this Range


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
dre27


Speed
0


Tactics
0






d
d


https://i0.wp.com/www.atlaslisboa.com/wp-content/uploads/2017/05/wolf-trap-icon-10.png
https://i0.wp.com/www.atlaslisboa.com/wp-content/uploads/2017/05/wolf-trap-icon-10.png


S
S


