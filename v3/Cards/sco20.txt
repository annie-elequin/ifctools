Title
Focus Fire


Class
Scourge


Order
5


Level
S


Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/scourge_focusfire/quality


Quantity
1


Spark
1


Cost



Power
0


OnSlot



OnUse
^Wound^Wound an Enemy
<br/>within (Range^Range^4)
<br/>
<br/>All Players can ^Move^Move 1-2 towards that Target and 
<br/>Gain ^Spark^Spark for doing so


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
sco20


Speed
0


Tactics
0






