Title
Sand Trap


Class
Foulborn


Level
1


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-TBjBfh8/0/ee38ee24/O/download%20%289%29.png


Quantity
1


Spark
1


Description
<img src="https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-nFPhkp6/0/8d2748e4/O/Foulborn_SandTrap.png" width="400" height="400"><br/>^Pin^Pin all Targets with any ^b^Block


Trigger



TriggerAction



Print
YES


Public
YES


id
fou24


Power
0


Speed
1


Tactics
0






