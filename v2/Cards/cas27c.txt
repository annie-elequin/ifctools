Title
Dagger Spray


Class
Cassius


Level
1


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-hFnXQRC/0/354b28bc/O/10426903.png


Quantity
1


Spark
2


Description
<img src="https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-zJT39N3/0/262135b8/O/Dreya_Avalanche_Arrow.png" width="400" height="400"><br/>^Discard^Discard up to (1+^spX^) cards<br/><br/>Deal ^d1^ to all Targets for each card Discarded


Trigger



TriggerAction



Print
YES


Public
YES


id
cas27c


Power



Speed



Tactics



