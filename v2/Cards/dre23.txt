Title
Master of Terrain


Class
Dreya


Level
S


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-dHFrLvS/0/bdb0058e/O/dreya_masterofterrain.png


Quantity
S


Spark
1


Description
^Move^Move up to (1+^SpeedX^)<br/>For each Enemy you can see within <br/>(^RangeLine^Line 5), gain ^b3^Block<br/><br/>If you Gain 0 Block, ^TopDeck^Topdeck this card and Gain ^Tactics^^Speed^


Trigger



TriggerAction



Print
YES


Public
YES


id
dre23


Power
0


Speed
2


Tactics
0






