Title
Blazing Barrage


Class
Scourge


Level
1


Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/scourge_blazingbarrageAOE/public


Quantity
1


Spark
3


Description
^Draw^Draw (1+^TacticsOne^), then ^Discard^Discard 1<br/><br/>Deal ^dX^ to ALL spaces (including yourself) equal to the Cost of the card Discarded


Trigger



TriggerAction



Print
YES


Public
YES


id
sco3


Power
3


Speed
0


Tactics
0






