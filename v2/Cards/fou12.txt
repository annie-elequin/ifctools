Title
Beat My Chest


Class
Foulborn


Level
1


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-DTrR9c4/0/6938595e/O/download%20%2811%29.png


Quantity
1


Spark
2


Description
Gain ^b4^Block<br/>Repeat this ^PowerX^ times<br/><br/>Then ^Taunt^Taunt all ^TargetAll^Enemies within (Range^Range^1), increasing this Range by 1 for each ^Power^ on this card


Trigger



TriggerAction



Print
YES


Public
YES


id
fou12


Power
1


Speed
0


Tactics
1






