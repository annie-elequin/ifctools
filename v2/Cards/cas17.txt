Title
Bribe


Class
Cassius


Level
1


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-Q8hNHNL/0/9743f928/O/Bribe.png


Quantity
1


Spark
0


Description
Place Tokens on this card equal to the cost of the top card in your Void Pile, then place that card in your Hand


Trigger



TriggerAction



Print
NO


Public
YES


id
cas17


Power
0


Speed
0


Tactics
1






