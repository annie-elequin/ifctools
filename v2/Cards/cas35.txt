Title
Sabotage


Class
Cassius


Level
1


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-bLh5Ws7/0/a4e8eb68/O/download%20%2829%29.png


Quantity
1


Spark
1


Description
Steal 0-4 ^b^Block from an ^TargetOne^Enemy within (^Range^2)<br/><br/>Apply ^Wound^Wound or ^Pin^Pin to them ^PowerX^ times


Trigger



TriggerAction



Print
NO


Public
YES


id
cas35


Power
1


Speed
0


Tactics
0






