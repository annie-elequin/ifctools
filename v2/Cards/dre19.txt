Title
Reviving Tip


Class
Dreya


Level
1


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-88grgzC/0/a6fb2df8/O/Screenshot%202023-01-27%20at%2011.05.41%20PM.png


Quantity
1


Spark
1


Description
^Draw^Draw 1<br/>The ^Next^Next card you play will swap all its final ^d^ Damage values for ^Heal^Heal values


Trigger



TriggerAction



Print
YES


Public
YES


id
dre19


Power
0


Speed
0


Tactics
1






