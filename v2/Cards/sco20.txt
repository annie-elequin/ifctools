Title
Focus Fire


Class
Scourge


Level
S


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Scourge/i-hXRNSBD/0/6c8c7b3b/O/focus%20fire.png


Quantity
1


Spark
1


Description
^Wound^Wound the lowest HP Unit <br/>within (^Range^4)<br/><br/>All Players may ^Move^Advance 1-2 on that Target and receive ^Power^ for doing so


Trigger



TriggerAction



Print
YES


Public
YES


id
sco20


Power
0


Speed
1


Tactics
1






