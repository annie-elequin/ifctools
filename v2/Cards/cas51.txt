Title
Charlatan Cloak


Class
Cassius


Level
2


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-SjdXG2q/0/c267a04e/O/Charlatan%20Cloak.png


Quantity
1


Spark
1


Description
Gain and Place 3 ^SpeedThree^ on this


Trigger
Reaction


TriggerAction
If you receive ^d^ Attack Dmg, you may Clear this to 


Print
NO


Public
NO


id
cas51


Power
0


Speed
1


Tactics
0






