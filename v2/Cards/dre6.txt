Title
Boxing Glove Arrow


Class
Dreya


Level
S


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-hmGkJHh/0/44231842/O/IFC%20%28Block%29.png


Quantity
1


Spark
1


Description
^SpeedOne^ = ^Move^Move 1 <br/>(^Line^Line 4): ^d3^ <br/>^PowerOne^=^d+2^<br/><br/>You may reduce the Base Damage on this to ^Push^Push X equal to unused Damage


Trigger



TriggerAction



Print
YES


Public
YES


id
dre6


Power
1


Speed
1


Tactics
0






