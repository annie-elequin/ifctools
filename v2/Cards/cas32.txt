Title
Perch


Class
Cassius


Level
1


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-dtCFpQg/0/27319a65/O/download%20%2813%29.png


Quantity
1


Spark
1


Description
Select the nearest Obstacle and ^Jump^Jump the shortest distance to an open space adjacent to it<br/><br/>Then Gain ^b4^Block<br/><br/>^SpeedOne^=Choose any open space adjacent to that Obstacle instead


Trigger



TriggerAction



Print
NO


Public
YES


id
cas32


Power
0


Speed
1


Tactics
0






