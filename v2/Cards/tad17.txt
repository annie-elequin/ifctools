Title
Drill Gun


Class
Tad


Level
2


Type
Card


Scale
0


Image
https://t3.ftcdn.net/jpg/00/54/75/56/360_F_54755691_2TIdE1de71Danlva2YDYH7nHLwfZW6UP.jpg


Quantity
2


Spark
2


Description
(R5, Line) ^d3^ and **Push** the **Target** to the attack's furthest range if possible.


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
NO


id
tad17


