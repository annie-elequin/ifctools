Title
Chain Leash


Class
Cassius


Level
1


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-XthFVfL/0/781e66c4/O/chain%20leash.png


Quantity
1


Spark
1


Description
(^Line^Line 3): ^d2^<br/><br/>Choose to ^Pull^Pull 1 to the Target or<br/>^Pull^Pull yourself until you are adjacent to the Target


Trigger



TriggerAction



Print
NO


Public
YES


id
cas18


Power
0


Speed
1


Tactics
1






