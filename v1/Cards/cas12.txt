Title
Nimble Stab


Class
Cassius


Level
0


Type
Card


Scale
30


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-6mRpSKg/0/1d53b2d5/O/download%20%287%29.png


Quantity
2


Spark
1


Description
^Move^Move 0-1 <br/>(^Range^**1**):^d1^<br/><br/>On ^Hit^Hit, ^Move^Move 0-1


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
cas12


