Title
Cruel Arrow


Class
Dreya


Level
2


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-KWFpTb5/0/9f95e2ba/O/dreya_cruelarrow.png


Quantity
1


Spark
1


Description
(^Range^3/4): ^d2^ <br/> If Target has half or less HP, deal this damage Twice


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
dre25


