Title
Jagged Tip


Class
Dreya


Level
1


Type
Card


Scale
0


Image
https://img.freepik.com/premium-vector/spear-logo-icon-vector-image_665655-6288.jpg?w=2000


Quantity
1


Spark
1


Description
On the ^Next^Next card you play, choose:<br/><br/>^Pierce^Pierce on all Damage Values<br/>OR<br/>Add ^d+2^ to all Damage Values


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
dre12


