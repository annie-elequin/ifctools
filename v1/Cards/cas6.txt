Title
Fright or Flight


Class
Cassius


Level
0


Type
Card


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-7ZC8nxG/0/479b669b/O/download.png


Quantity
1


Spark
1


Description
^Move^Move 0-3<br/><br/>OR<br/><br/>^Jump^Jump exactly 4 spaces <br/> in a ^Line^Line


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
cas6


