Title
Frag Fist


Class
Scourge


Level
0


Type
Card


Scale
0


Image
https://cdn4.iconfinder.com/data/icons/various-similars-glyph-silhouettes/300/185733545Untitled-3-512.png


Quantity
1


Spark
1


Description
^d2^ to an adjacent Enemy<br/><br/>If they took more than 2 Damage,<br/>^Push^Push X in a ^Line^Line <br/>equal to that additional Damage


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
sco19


