Title
Pin The Blame


Class
Cassius


Level
2


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-4Qm5TjT/0/83d8260c/O/download%20%2821%29.png


Quantity
1


Spark
2


Description
(^Range^2/3): Deal up to ^d3^<br/>Repeat for a second Target in Range<br/><br/>If eactly one Target lost 0 HP, they will ^Taunt^Taunt the other Unit immediately


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
cas22


